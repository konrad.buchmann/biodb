# BioDB Project WS 21/22
Konrad Buchmann

## Quick Start
Assuming you have Rust installed:
```
$ cargo build --release && target/release/biodb <FASTA> <GTF> <MNASE_SEQ> --total-gc --promotor-gc --promotor-nsome-affinity --tfbs-nsome-affinity
```

## Prerequisites
A reasonably recent Rust tollchain (tested with 1.59).

The easiest way to install the toolchain is with the `rustup` tool. Install it via your OS's package manager, or by following the [official instructions](https://www.rust-lang.org/tools/install). Now you can install/update your toolchain using

```
$ rustup install stable
or
$ rustup update stable
```

## Build
```
$ cargo build --release
```
Dependencies will be downloaded, compiled and statically linked automatically by cargo.
The binary will be located at `target/release/biodb`

## Usage
The program contains multiple parts which can be executed separately. Each part producing one plot used in the final report (in the current directory).

Usage is best summarized with the `--help` option:

```
$ biodb --help
biodb 
Konrad Buchmann

USAGE:
    biodb [OPTIONS] <SEQUENCE> <ANNOTATIONS> <MNASE_SEQ>

ARGS:
    <SEQUENCE>       File contatining a sequence for a single chromosome, in FASTA format
                     (.fa/.fasta)
    <ANNOTATIONS>    File contatining sequence annotations in (gz compressed) GTF format
                     (.gtf/.gtf.gz)
    <MNASE_SEQ>      File containing MNase-seq data in BigWig format (.bigWig)

OPTIONS:
    -h, --help                       Print help information
        --promotor-gc                Plot the average GC content of all promotor regions
        --promotor-nsome-affinity    Plot the average Nucleosome affinity of of all promotor regions
        --tfbs-nsome-affinity        Plot the average Nucleosome affinity of the TFBS
        --total-gc                   Plot the GC content for the whole chromosome
```
